const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        bundle: './src/index.jsx',
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: '[name].js'
    }, 
    devServer: {
      contentBase: './public',
    },
    module: {
        rules: [
            {
                test: /\.(jsx|js)$/,
                exclude: /service-worker.js/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react']
                    }
                }
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            chunks: ['bundle']
        }),
        new CopyPlugin({
            patterns: [
                {
                    from: path.resolve(__dirname, './src/manifest.json')
                },
                {
                    from: path.resolve(__dirname, './src/service-worker.js')
                },
                {
                    from: path.resolve(__dirname, 'src', 'images/icons'),
                    to: 'images/icons'
                }
            ]
        })
    ],
};