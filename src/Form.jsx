import React from 'react';
import FoodInput from './FoodInput.jsx';
import { sendRecord } from './api/ajax';

export default class Form extends React.Component {
    constructor(props) {
        super(props);
        this.getId = counterCreator();
        this.state = {
            time: '',
            food: [
                {
                    id: this.getId(),
                    name: '',
                    isProblem: false,
                    hasCompensation: false
                }
            ],
            place: '',
            comment: ''
        }
        this.submitHandle = (e) => {
            e.preventDefault();
            console.log(this.state);
            let response = sendRecord(this.props.url, this.state);
            response.then((result) => {
                console.log(result, 'success');
                this.setState({
                    time: '',
                    place: '',
                    comment: '',
                    food: [{
                        id: this.getId(),
                        name: '',
                        isProblem: false,
                        hasCompensation: false
                    }]
                });
            }).catch((err) => {
                console.log(err, 'error');
            })
            
        };
        this.commentChangeHandle = (e) => {
            this.setState({
                comment: e.target.value
            });
        }
        this.timeChangeHandle = (e) => {
            this.setState({
                time: e.target.value
            });
        }
        this.placeChangeHandle = (e) => {
            this.setState({
                place: e.target.value
            })
        }
        this.createNew = () => {
            this.setState({
                food: [
                    ...this.state.food,
                    {
                        id: this.getId(),
                        name: '',
                        isProblem: false,
                        hasCompensation: false
                    }
                ]
            });
        };
        this.foodChangeHandle = (item) => {
            this.setState({
                food: this.state.food.map((food) => {
                    if (item.id !== food.id) {
                        return food;
                    }
                    return item;
                })
            })
        }
        this.foodDeleteHandle = (id) => {
            this.setState({
                food: this.state.food.filter(item => item.id === id ? false : true)
            });
        }
    }

    render() {
        let foodList = this.state.food.map((item) => <FoodInput
            create={this.createNew}
            onChange={this.foodChangeHandle}
            deleteItem={this.foodDeleteHandle}
            food={item}
            key={item.id} 
        />);
        return (
            <form onSubmit={this.submitHandle} className='form'>
                <div className='input-container'>
                    <label className='input-container__label' htmlFor="time">Время</label>
                    <input type="time" name="time" onChange={this.timeChangeHandle} id="time" value={this.state.time}/>
                </div>
                <div className='input-container'>
                    {foodList}
                </div>
                <div className='input-container'>
                    <label className='input-container__label' htmlFor="place">Место</label>
                    <input type="text" name="place" id="place" onChange={this.placeChangeHandle} value={this.state.place} placeholder="Зал"/>
                </div>
                <div className='input-container'>
                    <label className='input-container__label' htmlFor="comment">Мысли и чувства</label>
                    <input type="text" id="comment" onChange={this.commentChangeHandle} value={this.state.comment} placeholder="Было хорошее настроение" />
                </div>
                
                <button onClick={this.submitHandle} type="submit">go</button>
            </form>
        )
    }
}

function counterCreator () {
    let counter = 0;

    return () => {
        return counter++;
    }
}