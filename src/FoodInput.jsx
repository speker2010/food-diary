import React from 'react';
import { useState } from 'react';

export default function FoodInput({ create, onChange, food, deleteItem }) {
    let [ untoched, setUntoched ] = useState(!food.name.length);
    let onFocus = () => {
        if (untoched) {
            create();
            setUntoched(false);
        }
    };
    return (
        <div>
            <div>
                <label>
                    <input type="checkbox" onFocus={onFocus} onChange={(e) => {
                        onChange({
                            ...food,
                            isProblem: !food.isProblem
                        });
                    }} name="" id="" value={food.isProblem}/>
                    Не желательно?
                </label>
                <label>
                    <input type="checkbox" onFocus={onFocus} onChange={(e) => {
                        onChange({
                            ...food,
                            hasCompensation: !food.hasCompensation
                        });
                    }} name="" id="" value={food.hasCompensation}/>
                    Компенсация?
                </label>
            </div>
            <input type="text" onFocus={onFocus} onChange={(e) => {
                onChange({
                    ...food,
                    name: e.target.value
                });
            }} value={food.name}/>
            <button onClick={(e) => {
                e.preventDefault();
                if (untoched) {
                    return;
                }
                deleteItem(food.id);
            }}>x</button>
            
        </div>
    );
}