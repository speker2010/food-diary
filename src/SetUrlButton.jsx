import React from 'react';
import { useState } from 'react';

export default function SetUrlButton({ onSave }) {
    let [ url, setUrl ] = useState('');

    return (
        <form onSubmit={(e) => {e.preventDefault();onSave(url)}}>
            <div className="input-container">
                <label className="input-container__label">Введите Url</label>
            <input type="url" name="" onChange={(e) => {setUrl(e.target.value)}} id="" value={url}/>
            </div>
            <button type="submit">Сохранить</button>
        </form>
    );
}