import React from 'react';
import { useState } from 'react';
import InstallButton from './InstallButton.jsx';
import Form from './Form.jsx';
import SetUrlButton from './SetUrlButton.jsx';

export default function App() {
    let [ url, setUrl ] = useState(localStorage.getItem('url'));
    let setUrlStorage = (value) => {
        setUrl(value);
        localStorage.setItem('url', value);
    };

    return (<div>
        <h1>Food Diary</h1>
        <InstallButton/>
        {!url && <SetUrlButton onSave={setUrlStorage} />}
        {!!url && <Form url={url}/>}
    </div>);
}