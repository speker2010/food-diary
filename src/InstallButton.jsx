import React from 'react';
import { useEffect, useState } from 'react';

export default function InstallButton() {
    let [savedEvent, setSavedEvent] = useState(null);

    useEffect(() => {
        const eventHandler = (e) => {
            setSavedEvent(e);
        };
        window.addEventListener('beforeinstallprompt', eventHandler);
        return () => {
            window.removeEventListener('beforeinstallprompt', eventHandler);
        }
    });
    
    return (
        !!savedEvent&&<button onClick={() => {
            savedEvent.prompt();
        }}>
            Install
        </button>
    );
}