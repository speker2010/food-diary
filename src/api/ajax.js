import axios from 'axios';

export function sendRecord(url, record) {
    return axios.post(url, record);
}